<?php

namespace app\controllers;

use app\models\StartForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\HttpException;
use app\models\QuestionForm;
use app\models\ResultForm;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['start-test', 'answer', 'start-test-again'],
                'rules' => [
                    [
                        'actions' => ['start-test'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['answer'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['start-test-again'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['get'],
                    'start-test' => ['post'],
                    'answer' => ['post'],
                    'start-test-again' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            /*'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],*/
        ];
    }

    /**
     * Выдаем стартовую форму или соотв.данные при перезагрузке страницы
     * @return string
     */
    public function actionIndex()
    {
        // Если перезагрузка страницы - показываем вопрос или результат теста
        if (!Yii::$app->user->isGuest) {

            // в зависимости от стадии теста выдаем следующий вопрос или результат

            // тест окончен
            if (\Yii::$app->SkyengService->testIsFinished) {
                // получаем результат
                $ResultForm = \Yii::$app->SkyengService->getResult();

                return $this->render('result', [
                    'ResultForm' => $ResultForm
                ]);
            }

            // тест продолжается

            // получаем вопрос, повторяем, а не задаем новый
            $QuestionForm = \Yii::$app->SkyengService->getQuestion(true);

            return $this->render('question', [
                'QuestionForm' => $QuestionForm
            ]);
        }

        // Иначе показываем стартовую форму
        $StartForm = new StartForm();

        return $this->render('index', [
            'StartForm' => $StartForm
        ]);

    }

    /**
     * Регистрируем юзера и задаем первый вопрос.
     * @return string
     * @throws HttpException
     */
    public function actionStartTest()
    {
        // валидируем имя юзера
        $StartForm = new StartForm();
        if (!$StartForm->load(Yii::$app->request->post()) || !$StartForm->validate()) {
            throw new HttpException(500, \Yii::t('app', 'Sorry, there are some problems'));
        }

        // регистрируем юзера
        \Yii::$app->SkyengService->registerUser($StartForm);

        // получаем первый вопрос
        $QuestionForm = \Yii::$app->SkyengService->getQuestion();

        return $this->renderAjax('question', [
            'QuestionForm' => $QuestionForm
        ]);
    }

    /**
     * Проверяем ответ юзера и выдаем новый вопрос или результат теста
     * @return string
     * @throws HttpException
     */
    public function actionAnswer()
    {
        // валидируем ответ юзера
        $QuestionForm = new QuestionForm();
        if (!$QuestionForm->load(Yii::$app->request->post()) || !$QuestionForm->validate()) {
            throw new HttpException(500, \Yii::t('app', 'Sorry, there are some problems'));
        }

        // обрабатываем ответ юзера
        \Yii::$app->SkyengService->handleAnswer($QuestionForm);

        // в зависимости от стадии теста выдаем следующий вопрос или результат

        // тест окончен
        if (\Yii::$app->SkyengService->testIsFinished) {
            // получаем результат
            $ResultForm = \Yii::$app->SkyengService->getResult();

            return $this->renderAjax('result', [
                'ResultForm' => $ResultForm
            ]);
        }

        // тест продолжается

        // получаем вопрос
        $QuestionForm = \Yii::$app->SkyengService->getQuestion();

        return $this->renderAjax('question', [
            'QuestionForm' => $QuestionForm
        ]);
    }

    /**
     * Разлогиниваем юзера и показываем стартовую страницу
     * @return string
     */
    public function actionStartTestAgain()
    {
        // разлогиниваем юзера
        \Yii::$app->SkyengService->logoutUser();

        $StartForm = new StartForm();

        return $this->renderAjax('index', [
            'StartForm' => $StartForm
        ]);
    }
}
