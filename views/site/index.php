<?php

/* @var $this yii\web\View */
/* @var $StartForm app\models\StartForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\SkyengService;

$this->title = 'My Yii Application';

/*$js = <<<'EOD'

var $form   = $('#start-form'),
    action  = $form.attr("action"),
    data    = $form.serialize();



    $form.on('beforeSubmit', function () {
        $.post(action, data)
            .done(function() {
                alert( "success" );
            })
            .fail(function() {
                alert( "error" );
            })
            .always(function() {
                alert( "finished" );
            });
        return false;
    });

EOD;

$this->registerJs($js);*/
?>

<section id="container">

    <?php $Form = ActiveForm::begin([
        'id' => 'form',
        'action' => Url::to(['site/start-test']),
        'options' => ['class' => 'form-horizontal'],
    ]); ?>

    <h3 class="form-text"><?= Html::encode(Yii::t('app', 'Enter name')) ?></h3>

    <?= $Form->field($StartForm, 'name')->textInput(['autofocus' => true, 'maxlength' => 255])->label(false) ?>

    <?= Html::button(Yii::t('app', 'Start test'), ['class' => 'btn btn-primary center-block submitter']) ?>

    <?php ActiveForm::end(); ?>

</section>
