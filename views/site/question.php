<?php

/* @var $this yii\web\View */
/* @var $QuestionForm QuestionForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\SkyengService;
use app\models\QuestionForm;

$this->title = 'My Yii Application';

?>

<section id="container">

    <?php $Form = ActiveForm::begin([
        'id' => 'form',
        'action' => Url::to(['site/answer']),
    ]); ?>

    <?php if ($QuestionForm->errorMessage): ?>
    <div class="alert alert-danger">
        <?= Html::encode($QuestionForm->errorMessage) ?>
    </div>
    <?php endif; ?>

    <h3 class="form-text"><?= Html::encode($QuestionForm->name).', ' ?><span class="label label-default"><?= Html::encode($QuestionForm->word) ?></span><?= Html::encode(' - '.Yii::t('app', 'is')) ?></h3>

    <div class="panel panel-default">
        <div class="panel-body">
            <?= $Form->field($QuestionForm, 'answer')->radioList($QuestionForm->options)->label(false) ?>
        </div>
    </div>

    <?= Html::button(Yii::t('app', 'To answer'), ['class' => 'btn btn-primary center-block submitter']) ?>

    <?php ActiveForm::end(); ?>

</section>
