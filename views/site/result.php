<?php

/* @var $this yii\web\View */
/* @var $ResultForm ResultForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use app\components\SkyengService;
use app\models\ResultForm;

$this->title = 'My Yii Application';

?>

<section id="container">

    <?php $Form = ActiveForm::begin([
        'id' => 'form',
        'action' => Url::to(['site/start-test-again']),
    ]); ?>

    <h3 class="form-text">
        <?= Html::encode(
            $ResultForm->name.', '.Yii::t('app', 'You have got')
            .' '.Yii::t('app', '{n, plural, =0{0 points} =1{1 point} other{# points}}', ['n' => $ResultForm->score])
            .' '.Yii::t('app', 'and made').' '.Yii::t('app', '{n, plural, =0{0 mistakes} =1{1 mistake} other{# mistakes}}', ['n' => $ResultForm->mistakes]).'.'
        ) ?>
    </h3>

    <?= Html::button(Yii::t('app', 'Start test again'), ['class' => 'btn btn-primary center-block submitter']) ?>

    <?php ActiveForm::end(); ?>

</section>
