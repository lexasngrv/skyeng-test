<?php

use yii\db\Migration;

/**
 * Handles the creation for table `progress`.
 */
class m160604_170322_create_progress extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('progress', [
            'users_id' => $this->integer()->unsigned(),
            'dictionary_id' => $this->integer()->unsigned(),
            'direction' => $this->boolean(),
            'created_at' => $this->integer()->unsigned(),
            'updated_at' => $this->integer()->unsigned(),
            'PRIMARY KEY(users_id)',
        ]);

        // creates index for column `users_id`
        /*$this->createIndex(
            'idx-progress-users_id',
            'progress',
            'users_id'
        );*/

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-progress-users_id',
            'progress',
            'users_id',
            'users',
            'id',
            'CASCADE'
        );

        // creates index for column `dictionary_id`
        $this->createIndex(
            'idx-progress-dictionary_id',
            'progress',
            'dictionary_id'
        );

        // add foreign key for table `dictionary`
        $this->addForeignKey(
            'fk-progress-dictionary_id',
            'progress',
            'dictionary_id',
            'dictionary',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-progress-users_id',
            'progress'
        );

        // drops index for column `users_id`
        /*$this->dropIndex(
            'idx-progress-users_id',
            'progress'
        );*/

        // drops foreign key for table `dictionary`
        $this->dropForeignKey(
            'fk-progress-dictionary_id',
            'progress'
        );

        // drops index for column `dictionary_id`
        $this->dropIndex(
            'idx-progress-dictionary_id',
            'progress'
        );

        $this->dropTable('progress');
    }
}
