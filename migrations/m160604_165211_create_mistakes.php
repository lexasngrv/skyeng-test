<?php

use yii\db\Migration;

/**
 * Handles the creation for table `mistakes`.
 */
class m160604_165211_create_mistakes extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('mistakes', [
            'id' => $this->primaryKey(),
            'users_id' => $this->integer()->unsigned(),
            'words_en_id' => $this->integer()->unsigned(),
            'words_ru_id' => $this->integer()->unsigned(),
            'direction' => $this->boolean(),
            'created_at' => $this->integer()->unsigned(),
            'updated_at' => $this->integer()->unsigned(),
        ]);

        // creates index for column `users_id`
        $this->createIndex(
            'idx-mistakes-users_id',
            'mistakes',
            'users_id'
        );

        // add foreign key for table `users`
        $this->addForeignKey(
            'fk-mistakes-users_id',
            'mistakes',
            'users_id',
            'users',
            'id',
            'CASCADE'
        );

        // creates index for column `words_en_id`
        $this->createIndex(
            'idx-mistakes-words_en_id',
            'mistakes',
            'words_en_id'
        );

        // add foreign key for table `words_en`
        $this->addForeignKey(
            'fk-mistakes-words_en_id',
            'mistakes',
            'words_en_id',
            'words_en',
            'id',
            'CASCADE'
        );

        // creates index for column `words_ru_id`
        $this->createIndex(
            'idx-mistakes-words_ru_id',
            'mistakes',
            'words_ru_id'
        );

        // add foreign key for table `words_ru`
        $this->addForeignKey(
            'fk-mistakes-words_ru_id',
            'mistakes',
            'words_ru_id',
            'words_ru',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `users`
        $this->dropForeignKey(
            'fk-mistakes-users_id',
            'mistakes'
        );

        // drops index for column `users_id`
        $this->dropIndex(
            'idx-mistakes-users_id',
            'mistakes'
        );

        // drops foreign key for table `words_en`
        $this->dropForeignKey(
            'fk-mistakes-words_en_id',
            'mistakes'
        );

        // drops index for column `words_en_id`
        $this->dropIndex(
            'idx-mistakes-words_en_id',
            'mistakes'
        );

        // drops foreign key for table `words_ru`
        $this->dropForeignKey(
            'fk-mistakes-words_ru_id',
            'mistakes'
        );

        // drops index for column `words_ru_id`
        $this->dropIndex(
            'idx-mistakes-words_ru_id',
            'mistakes'
        );

        $this->dropTable('mistakes');
    }
}
