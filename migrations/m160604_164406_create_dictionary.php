<?php

use yii\db\Migration;

/**
 * Handles the creation for table `dictionary`.
 */
class m160604_164406_create_dictionary extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('dictionary', [
            'id' => $this->primaryKey(),
            'words_en_id' => $this->integer()->unsigned(),
            'words_ru_id' => $this->integer()->unsigned(),
            'created_at' => $this->integer()->unsigned(),
            'updated_at' => $this->integer()->unsigned(),
            //'PRIMARY KEY(words_en_id, words_ru_id)',
        ]);

        // creates index for column `words_en_id`
        $this->createIndex(
            'idx-dictionary-words_en_id',
            'dictionary',
            'words_en_id'
        );

        // add foreign key for table `words_en`
        $this->addForeignKey(
            'fk-dictionary-words_en_id',
            'dictionary',
            'words_en_id',
            'words_en',
            'id',
            'CASCADE'
        );

        // creates index for column `words_ru_id`
        $this->createIndex(
            'idx-dictionary-words_ru_id',
            'dictionary',
            'words_ru_id'
        );

        // add foreign key for table `words_ru`
        $this->addForeignKey(
            'fk-dictionary-words_ru_id',
            'dictionary',
            'words_ru_id',
            'words_ru',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `words_en`
        $this->dropForeignKey(
            'fk-dictionary-words_en_id',
            'dictionary'
        );

        // drops index for column `words_en_id`
        $this->dropIndex(
            'idx-dictionary-words_en_id',
            'dictionary'
        );

        // drops foreign key for table `words_ru`
        $this->dropForeignKey(
            'fk-dictionary-words_ru_id',
            'dictionary'
        );

        // drops index for column `words_ru_id`
        $this->dropIndex(
            'idx-dictionary-words_ru_id',
            'dictionary'
        );

        $this->dropTable('dictionary');
    }
}
