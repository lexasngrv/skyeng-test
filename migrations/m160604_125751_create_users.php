<?php

use yii\db\Migration;

/**
 * Handles the creation for table `users`.
 */
class m160604_125751_create_users extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'mistakes' => $this->smallInteger()->unsigned(),
            'score' => $this->integer()->unsigned(),
            'access_token' => $this->string()->unique(),
            'auth_key' => $this->string()->unique(),
            'created_at' => $this->integer()->unsigned(),
            'updated_at' => $this->integer()->unsigned(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
