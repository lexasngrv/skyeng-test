$(document).ready(function () {

    $(document).on('submit', '#form', function () {

        var $form = $(this),
            action = $form.attr("action"),
            data = $form.serialize(),
            $container = $('#container'),
            spinnerTarget = document.getElementById('wrap');

        var spinner = new Spinner().spin(spinnerTarget);

        $.post(action, data)
            .done(function(data) {
                $container.html(data)
            })
            .fail(function(qXHR, textStatus, errorThrown) {
                $container.html(qXHR.responseText)
            }).always(function(data) {
                spinner.stop();
            });

        return false;
    });

    $(document).on("click", ".submitter", function() {
        $("#form").submit();
        return false;
    });
});

