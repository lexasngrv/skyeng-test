<?php
return [
    'Name' => 'Имя',
    'Start test' => 'Начать тест',
    'Sorry, there are some problems' => 'Извините, возникли небольшие проблемы',
    'Hello' => 'Здравствуйте',
    'is' => 'это',
    'Answer' => 'Ответ',
    '{n, plural, =0{0 points} =1{1 point} other{# points}}' => '{n, plural, =0{0 баллов} =1{1 балл} one{# балл} few{# балла} many{# баллов} other{# балла}}',
    'You have got' => 'Вы набрали',
    'You have made' => 'Вы сделали',
    '{n, plural, =0{0 mistakes} =1{1 mistake} other{# mistakes}}' => '{n, plural, =0{0 ошибок} =1{1 ошибку} one{# ошибка} few{# ошибки} many{# ошибок} other{# ошибки}}',
    'Start test again' => 'Начать тест заново',
    'Mistake! Try again!' => 'Ошибка! Попробуйте еще раз!',
    'Enter name' => 'Введите имя',
    'To answer' => 'Ответить',
    'and made' => 'и сделали',
];