<?php

namespace app\components;

use yii\base\Component;
use app\models\StartForm;
use yii\web\HttpException;
use app\models\Dictionary;
use app\models\Progress;
use app\models\User;
use app\models\QuestionForm;
use app\models\ResultForm;
use app\models\Mistake;

/**
 * Сервис со всей бизнес-логикой.
 * @package app\components
 */
class SkyengService extends Component
{
    /**
     * Направление перевода en -> ru
     */
    const TRANSLATION_DIRECTION_EN_RU = 1;
    /**
     * Направление перевода ru -> en
     */
    const TRANSLATION_DIRECTION_RU_EN = 0;
    /**
     * Имя ключа для хранения формы вопроса во флэш
     */
    const QUESTION_FORM_FLASH_KEY = 'QuestionForm';

    /**
     * @var bool повторить ли вопрос
     */
    private $_repeatQuestion = false;

    /**
     * Получаем вопрос.
     * @param null|bool $repeatQuestion повторить ли вопрос
     * @return QuestionForm
     */
    public function getQuestion($repeatQuestion = null)
    {
        // если надо повторить вопрос - достаем форму вопроса из флэш
        if ($this->_repeatQuestion || $repeatQuestion) {
            $QuestionForm = $this->getQuestionFormFromFlash();

            return $QuestionForm;
        }

        // выбираем случайным образом направление перевода
        $translationDirection = $this->getTranslationDirectionRandomly();

        // достаем основную пару слов под перевод
        $PairToTranslate = $this->getPairToTranslate();

        // обновляем прогресс юзера
        $this->updateUserProgress($PairToTranslate, $translationDirection);

        // достаем еще 3 случайных пары
        $RandomPairs = Dictionary::find()->where('id <> :id', [':id' => $PairToTranslate->id])->orderBy('random()')->limit(3)->all(); // TODO random() - postgresql specific

        // готовим форму вопроса
        $QuestionForm = $this->getQuestionForm($PairToTranslate, $RandomPairs, $translationDirection);

        // сохраняем форму во флэш для возможности повторить вопрос
        \Yii::$app->session->setFlash(self::QUESTION_FORM_FLASH_KEY, serialize($QuestionForm));

        return $QuestionForm;
    }

    /**
     * @return QuestionForm
     */
    protected function getQuestionFormFromFlash()
    {
        /* @var QuestionForm $QuestionForm*/
        $QuestionForm = \Yii::$app->session->getFlash(self::QUESTION_FORM_FLASH_KEY);

        // пересохраняем форму во флэш для возможности повторить вопрос еще раз
        \Yii::$app->session->setFlash(self::QUESTION_FORM_FLASH_KEY, $QuestionForm);

        $QuestionForm = unserialize($QuestionForm);

        return $QuestionForm;
    }

    /**
     * @return array|null|\yii\db\ActiveRecord
     */
    protected function getPairToTranslate()
    {
        $Progress = \Yii::$app->user->getIdentity()->progress;

        if ($Progress) { // если это не первый вопрос - нам нужно следующее слово по списку
            $PairToTranslate = Dictionary::find()->where('id > :id', [':id' => $Progress->dictionary_id])->orderBy('id ASC')->one();
        } else { // если первый - то первое слово
            $PairToTranslate = Dictionary::find()->orderBy('id ASC')->one();
        }

        return $PairToTranslate;
    }

    /**
     * @param Dictionary $PairToTranslate
     * @param $translationDirection
     */
    protected function updateUserProgress(Dictionary $PairToTranslate, $translationDirection)
    {
        $Progress = \Yii::$app->user->getIdentity()->progress;
        if (!$Progress) {
            $Progress = new Progress();
        }

        $Progress->users_id = \Yii::$app->user->getIdentity()->id;
        $Progress->dictionary_id = $PairToTranslate->id;
        $Progress->direction = $translationDirection;

        $Progress->save(false);
    }

    /**
     * @param Dictionary $PairToTranslate
     * @param array $RandomPairs
     * @param $translationDirection
     * @return QuestionForm
     */
    protected function getQuestionForm(Dictionary $PairToTranslate, array $RandomPairs, $translationDirection)
    {
        if ($translationDirection == self::TRANSLATION_DIRECTION_EN_RU) {
            $fromRelation = 'wordEn';
            $toRelation = 'wordRu';
        } else {
            $fromRelation = 'wordRu';
            $toRelation = 'wordEn';
        }

        $options = [
            $PairToTranslate->$toRelation->id => $PairToTranslate->$toRelation->name
        ];
        foreach ($RandomPairs as /** @var $RandomPair Dictionary */ $RandomPair) {
            $options[$RandomPair->$toRelation->id] = $RandomPair->$toRelation->name;
        }
        $this->shuffleAssocArray($options);

        $QuestionForm = new QuestionForm();
        $QuestionForm->name = \Yii::$app->user->getIdentity()->name;
        $QuestionForm->word = $PairToTranslate->$fromRelation->name;
        $QuestionForm->options = $options;

        return $QuestionForm;
    }

    /**
     * @param $array
     * @return bool
     */
    protected function shuffleAssocArray(&$array)
    {
        $keys = array_keys($array);

        shuffle($keys);

        $new = [];
        foreach($keys as $key) {
            $new[$key] = $array[$key];
        }
        $array = $new;

        return true;
    }

    /**
     * Обрабоатываем овтет на вопрос
     * @param QuestionForm $QuestionForm
     */
    public function handleAnswer(QuestionForm $QuestionForm)
    {
        // проверяем, правильно ли ответил юзер
        $answerIsCorrect = $this->checkIfAnswerIsCorrect($QuestionForm);

        // обновляем счетчики баллов и ошибок юзера
        $this->updateUserScoreAndMistakes($answerIsCorrect);

        // проверяем, остались ли непройденные слова
        $unprocessedWordsRemain = $this->checkIfUnprocessedWordsRemain();

        $User = \Yii::$app->user->getIdentity();

        // решаем, что делать дальше - заканчивать тест или задавать след.вопрос
        if ($answerIsCorrect && !$unprocessedWordsRemain) { // ответ верный, но закончились слова - заканчиваем тест
            $this->finishTest();
        } elseif (!$answerIsCorrect && $User->mistakes == 3) { // ответ неверный, и 3 ошибки - сохраняем ошибку и заканчиваем тест
            $this->saveMistake($QuestionForm);
            $this->finishTest();
        } elseif (!$answerIsCorrect && $User->mistakes < 3) { // ответ неверный, и меньше 3х ошибок - сохраняем ошибку и продолжаем тест, но повторяем вопрос
            $this->saveMistake($QuestionForm);
            $this->_repeatQuestion = true;
            // добавим сообщение об ошибке в форму вопроса
            $this->addErrorMessageToQuestionFormInFlash();
        } // остался сценарий когда ответ верный и не закончились слова - но тогда продолжаем тест, по умолчанию
    }

    protected function addErrorMessageToQuestionFormInFlash()
    {
        /* @var QuestionForm $QuestionForm*/
        $QuestionForm = unserialize(\Yii::$app->session->getFlash(self::QUESTION_FORM_FLASH_KEY));

        $QuestionForm->errorMessage = \Yii::t('app', 'Mistake! Try again!');

        \Yii::$app->session->setFlash(self::QUESTION_FORM_FLASH_KEY, serialize($QuestionForm));
    }

    protected function finishTest()
    {
        $Progress = \Yii::$app->user->getIdentity()->progress;
        $Progress->delete();
    }

    protected function saveMistake(QuestionForm $QuestionForm)
    {
        $User = \Yii::$app->user->getIdentity();
        $Progress = $User->progress;
        $PairToTranslate = $Progress->dictionary;
        $translationDirection = $Progress->direction;

        $Mistake = new Mistake();
        $Mistake->users_id = $User->id;
        $Mistake->words_en_id = ($translationDirection == self::TRANSLATION_DIRECTION_EN_RU ? $PairToTranslate->words_en_id : $QuestionForm->answer);
        $Mistake->words_ru_id = ($translationDirection == self::TRANSLATION_DIRECTION_EN_RU ? $QuestionForm->answer : $PairToTranslate->words_ru_id);
        $Mistake->direction = $translationDirection;

        $Mistake->save(false);
    }

    protected function checkIfUnprocessedWordsRemain()
    {
        $Progress = \Yii::$app->user->getIdentity()->progress;

        $countPairsToTranslate = Dictionary::find()->where('id > :id', [':id' => $Progress->dictionary_id])->count();

        return $countPairsToTranslate > 0;
    }

    protected function updateUserScoreAndMistakes($answerIsCorrect)
    {
        /* @var User */
        $User = \Yii::$app->user->getIdentity();

        if ($answerIsCorrect) {
            $User->score++;
        } else {
            $User->mistakes++;
        }

        $User->save(false);
    }

    protected function checkIfAnswerIsCorrect(QuestionForm $QuestionForm)
    {
        $User = \Yii::$app->user->getIdentity();
        $Progress = $User->progress;
        $PairToTranslate = $Progress->dictionary;
        $translationDirection = $Progress->direction;

        $answerIsCorrect =
            $translationDirection == self::TRANSLATION_DIRECTION_EN_RU
            ? $PairToTranslate->words_ru_id == $QuestionForm->answer
            : $PairToTranslate->words_en_id == $QuestionForm->answer
        ;

        return $answerIsCorrect;
    }

    /**
     * Получаем результат теста.
     * @return ResultForm
     */
    public function getResult()
    {
        $User = \Yii::$app->user->getIdentity();

        $ResultForm = new ResultForm();
        $ResultForm->name = $User->name;
        $ResultForm->score = $User->score;
        $ResultForm->mistakes = $User->mistakes;

        return $ResultForm;
    }

    /**
     * Регистрируем юзера и логиним его.
     * @param StartForm $StartForm
     * @throws HttpException
     */
    public function registerUser(StartForm $StartForm)
    {
        /** @var User */
        $User = new User();
        $User->name = $StartForm->name;
        $User->mistakes = 0;
        $User->score = 0;
        $User->access_token = \Yii::$app->security->generateRandomString();
        $User->auth_key = \Yii::$app->security->generateRandomString();
        if (!$User->save()) {
            throw new HttpException(500, \Yii::t('app', 'Sorry, there are some problems'));
        }

        \Yii::$app->user->login($User);
    }

    /**
     * Разлогиниваем юзера.
     */
    public function logoutUser()
    {
        \Yii::$app->user->logout();
    }

    protected function getTranslationDirectionRandomly()
    {
        return array_rand([self::TRANSLATION_DIRECTION_EN_RU, self::TRANSLATION_DIRECTION_RU_EN]);
    }

    /**
     * Геттер.
     * @return boolean
     */
    public function getTestIsFinished()
    {
        // запрашиваем динамически, чтобы не получить положительный ответ, когда запись уже удалена из БД
        // см. lazy loading
        $Progress = \Yii::$app->user->getIdentity()->getProgress()->one();

        return empty($Progress);
    }
}