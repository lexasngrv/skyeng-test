<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Стартовая форма.
 * Для получения имени пользователя.
 */
class StartForm extends Model
{
    /**
     * @var string имя пользователя
     */
    public $name;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'trim'],
            ['name', 'string', 'max' => 255],
            ['name', 'required'],
        ];
    }
}
