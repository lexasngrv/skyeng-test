<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "progress".
 *
 * @property integer $users_id
 * @property integer $dictionary_id
 * @property boolean $direction
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Dictionary $dictionary
 * @property User $users
 * @property WordEn $wordsEn
 * @property WordRu $wordsRu
 */
class Progress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'progress';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id', 'dictionary_id', 'created_at', 'updated_at'], 'integer'],
            [['direction'], 'boolean'],
            [['dictionary_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dictionary::className(), 'targetAttribute' => ['dictionary_id' => 'id']],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['users_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'users_id' => Yii::t('app', 'Users ID'),
            'dictionary_id' => Yii::t('app', 'Dictionary ID'),
            'direction' => Yii::t('app', 'Direction'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDictionary()
    {
        return $this->hasOne(Dictionary::className(), ['id' => 'dictionary_id'])->inverseOf('progresses');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'users_id'])->inverseOf('progress');
    }

}
