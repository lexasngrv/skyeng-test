<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $name
 * @property integer $mistakes
 * @property integer $score
 * @property string $access_token
 * @property string $auth_key
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Progress[] $progresses
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mistakes', 'score', 'created_at', 'updated_at'], 'integer'],
            [['name', 'access_token', 'auth_key'], 'string', 'max' => 255],
            [['access_token'], 'unique'],
            [['auth_key'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'mistakes' => Yii::t('app', 'Mistakes'),
            'score' => Yii::t('app', 'Score'),
            'access_token' => Yii::t('app', 'Access Token'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgress()
    {
        // TODO may be use hasMany()
        return $this->hasOne(Progress::className(), ['users_id' => 'id'])->inverseOf('user');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMistakes()
    {
        return $this->hasMany(Mistake::className(), ['users_id' => 'id'])->inverseOf('user');
    }

    /**
     * @inheritdoc
     */
     public static function findIdentity($id)
     {
         return static::findOne($id);
     }

    /**
     * @inheritdoc
     */
     public static function findIdentityByAccessToken($token, $type = null)
     {
         return static::findOne(['access_token' => $token]);
     }

    /**
     * @inheritdoc
     */
     public function getId()
     {
         return $this->id;
     }

    /**
     * @inheritdoc
     */
     public function getAuthKey()
     {
         return $this->auth_key;
     }

    /**
     * @inheritdoc
     */
     public function validateAuthKey($authKey)
     {
         return $this->auth_key === $authKey;
     }
}
