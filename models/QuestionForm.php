<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Форма вопроса.
 * Используется как для валидации ответа, так и для вывода нового вопроса.
 */
class QuestionForm extends Model
{
    /**
     * @var integer выбранный ответ, айди слова
     */
    public $answer;
    /**
     * @var string имя юзера, НЕ аттрибут формы
     */
    public $name;
    /**
     * @var string слово, которое надо перевести, НЕ аттрибут формы
     */
    public $word;
    /**
     * @var array варианты переводов, НЕ аттрибут формы
     */
    public $options;
    /**
     * @var string сообщение об ошибке, НЕ аттрибут формы
     */
    public $errorMessage;

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return ['answer'];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'answer' => Yii::t('app', 'Answer'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['answer', 'integer'],
            ['answer', 'required'],
        ];
    }
}
