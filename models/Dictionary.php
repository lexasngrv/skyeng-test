<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dictionary".
 *
 * @property integer $id
 * @property integer $words_en_id
 * @property integer $words_ru_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property WordEn $wordsEn
 * @property WordRu $wordsRu
 * @property Progress[] $progresses
 */
class Dictionary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dictionary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['words_en_id', 'words_ru_id', 'created_at', 'updated_at'], 'integer'],
            [['words_en_id'], 'exist', 'skipOnError' => true, 'targetClass' => WordEn::className(), 'targetAttribute' => ['words_en_id' => 'id']],
            [['words_ru_id'], 'exist', 'skipOnError' => true, 'targetClass' => WordRu::className(), 'targetAttribute' => ['words_ru_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'words_en_id' => Yii::t('app', 'Words En ID'),
            'words_ru_id' => Yii::t('app', 'Words Ru ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWordEn()
    {
        return $this->hasOne(WordEn::className(), ['id' => 'words_en_id'])->inverseOf('dictionary');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWordRu()
    {
        return $this->hasOne(WordRu::className(), ['id' => 'words_ru_id'])->inverseOf('dictionary');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgresses()
    {
        return $this->hasMany(Progress::className(), ['dictionary_id' => 'id'])->inverseOf('dictionary');
    }
}
