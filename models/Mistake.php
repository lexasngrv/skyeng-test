<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mistakes".
 *
 * @property integer $id
 * @property integer $users_id
 * @property integer $words_en_id
 * @property integer $words_ru_id
 * @property boolean $direction
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property WordEn $wordsEn
 * @property WordRu $wordsRu
 */
class Mistake extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mistakes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['users_id', 'words_en_id', 'words_ru_id', 'created_at', 'updated_at'], 'integer'],
            [['direction'], 'boolean'],
            [['users_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['users_id' => 'id']],
            [['words_en_id'], 'exist', 'skipOnError' => true, 'targetClass' => WordEn::className(), 'targetAttribute' => ['words_en_id' => 'id']],
            [['words_ru_id'], 'exist', 'skipOnError' => true, 'targetClass' => WordRu::className(), 'targetAttribute' => ['words_ru_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'users_id' => Yii::t('app', 'Users ID'),
            'words_en_id' => Yii::t('app', 'Words En ID'),
            'words_ru_id' => Yii::t('app', 'Words Ru ID'),
            'direction' => Yii::t('app', 'Direction'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'users_id'])->inverseOf('mistakes');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWordEn()
    {
        return $this->hasOne(WordEn::className(), ['id' => 'words_en_id'])->inverseOf('mistakes');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWordRu()
    {
        return $this->hasOne(WordRu::className(), ['id' => 'words_ru_id'])->inverseOf('mistakes');
    }
}
