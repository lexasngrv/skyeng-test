<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Форма результата теста.
 * Без атрибутов формы, используется только для вывода результатов.
 */
class ResultForm extends Model
{
    /**
     * @var string имя юзера, НЕ аттрибут формы
     */
    public $name;
    /**
     * @var string кол-во баллов юзера, НЕ аттрибут формы
     */
    public $score;
    /**
     * @var array кол-во оштбок юзера, НЕ аттрибут формы
     */
    public $mistakes;

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [];
    }
}
